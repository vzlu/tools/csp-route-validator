#!/usr/bin/env python3

import sys
import yaml

def debug_print(val):
    debug = 1
    if (debug):
        print(val)

def parse_rtable(routes):
    for route in routes:
        if route["rtable"] == "":
            return
        rtable_list = []
        rtable = route["rtable"]
        rtable_vals = rtable.split(', ')
        for value in rtable_vals:
            split_val = value.split(' ')
            if len(split_val) == 3:
                rtable_list.append({"mask": split_val[0], "ifc": split_val[1], "via": split_val[2]})
            else:
                rtable_list.append({"mask": split_val[0], "ifc": split_val[1]})
        route["rtable"] = rtable_list
    return routes

def process_routes(yaml_object):
    name = ""
    node = ""
    interfaces = []
    rtable = ""
    routes_count = len(yaml_object["routes"])
    if (routes_count > 0):
        print("Routes loaded!")
        print("Count: " + str(routes_count))
    # Start checking the validity of the yaml
    for route in yaml_object["routes"]:
        name = route["name"]
        node = route["node"]
        interfaces = route["interfaces"]
        rtable = route["rtable"]
        if name == "":
            print("Route missing a name!")
            return
        if node == "":
            print("Route missing a node!")
            return
        if len(interfaces) <= 0:
            print("Route missing interfaces!")
            return
        if rtable == "":
            print("Route missing a routing table!")
            return
    #Everything is valid, Add the routes to the route object
    return yaml_object["routes"]

def get_info(routes, CSP_Node):
    for route in routes:
        if route["node"] == CSP_Node:
            print("CSP Node " + str(CSP_Node) + " Info:")
            print("Route name: " + str(route["name"]))
            print("Route interfaces: " + str(route["interfaces"]))
            print("Route routing table: " + str(route["rtable"]))
            return 0
    print("Node not found!")

def attempt_route(routes, from_node, to_node):
    print("Attempting to route between " + str(from_node) + " and " + str(to_node) + "!")
    route_from = {}
    route_to = {}
    for route in routes:
        if route["node"] == from_node:
            route_from = route
        if route["node"] == to_node:
            route_to = route
    if (route_from is {}) or (route_to is {}):
        print("One of the routes were not found!")
        return -1
    
    # Check if we share an interface
    from_interfaces = route_from["interfaces"]
    to_interfaces = route_to["interfaces"]
    shared_interfaces = []

    for interface_f in from_interfaces:
        for interface_t in to_interfaces:
            if interface_f["type"] == interface_t["type"]:
                shared_interfaces.append(interface_f)

    # Check if we have default route in the routing table, simplest scenario
    if (len(shared_interfaces) > 0):
        f_rtable = route_from["rtable"]
        for interface in shared_interfaces:
            for route in f_rtable:
                if (interface["name"] == route["ifc"]):
                    mask = route["mask"]
                    avail_routes = []
                    if (mask == "0/0"):
                        print("Route found through " + str(route))
                        return 0        
                    # No default route completes the check, try masked routes with shared interfaces
                    mask_vals = mask.split('/')
                    match int(mask_vals[1]):
                        case 5:
                            # direct route only
                            #avail_routes.append(int(mask_vals[0]))
                            if (to_node == int(mask_vals[0])):
                                print("Route found through " + str(route))
                                return 0
                        #Match 1 MSB
                        case 1:
                            masked_to = to_node & 0b10000
                            masked_rt = int(mask_vals[0]) & 0b10000
                            if (masked_to == masked_rt):
                                print("Route found through " + str(route))
                                return 0
                        #Match 2 MSB
                        case 2:
                            masked_to = to_node & 0b11000
                            masked_rt = int(mask_vals[0]) & 0b11000
                            if (masked_to == masked_rt):
                                print("Route found through " + str(route))
                                return 0
                        #Match 3 MSB
                        case 3:
                            masked_to = to_node & 0b11100
                            masked_rt = int(mask_vals[0]) & 0b11100
                            if (masked_to == masked_rt):
                                print("Route found through " + str(route))
                                return 0
                        #Match 4 MSB
                        case 4:
                            masked_to = to_node & 0b11110
                            masked_rt = int(mask_vals[0]) & 0b11110
                            if (masked_to == masked_rt):
                                print("Route found through " + str(route))
                                return 0
    # Look for bridges
    else:
        f_rtable = route_from["rtable"]
        for interface_f in from_interfaces:
            for route in f_rtable:
                if ("via" in route):
                    if (interface_f["name"] == route["ifc"]):
                        mask = route["mask"]
                        avail_routes = []
                        if (mask == "0/0"):
                            print("Route found through " + str(route))
                            attempt_route(routes, int(route["via"]), to_node)  
                            return 0         
                        # No default route completes the check, try masked routes with shared interfaces
                        mask_vals = mask.split('/')
                        match int(mask_vals[1]):
                            case 5:
                                # direct route only
                                #avail_routes.append(int(mask_vals[0]))
                                if (to_node == int(mask_vals[0])):
                                    print("Route found through " + str(route))
                                    attempt_route(routes, int(route["via"]), to_node)  
                                    return 0
                            #Match 1 MSB
                            case 1:
                                masked_to = to_node & 0b10000
                                masked_rt = int(mask_vals[0]) & 0b10000
                                if (masked_to == masked_rt):
                                    print("Route found through " + str(route))
                                    attempt_route(routes, int(route["via"]), to_node) 
                                    return 0
                            #Match 2 MSB
                            case 2:
                                masked_to = to_node & 0b11000
                                masked_rt = int(mask_vals[0]) & 0b11000
                                if (masked_to == masked_rt):
                                    print("Route found through " + str(route))
                                    attempt_route(routes, int(route["via"]), to_node)
                                    return 0
                            #Match 3 MSB
                            case 3:
                                masked_to = to_node & 0b11100
                                masked_rt = int(mask_vals[0]) & 0b11100
                                if (masked_to == masked_rt):
                                    print("Route found through " + str(route))
                                    attempt_route(routes, int(route["via"]), to_node)  
                                    return 0
                            #Match 4 MSB
                            case 4:
                                masked_to = to_node & 0b11110
                                masked_rt = int(mask_vals[0]) & 0b11110
                                if (masked_to == masked_rt):
                                    print("Route found through " + str(route))
                                    attempt_route(routes, int(route["via"]), to_node)
                                    return 0
                    

    print("Route not found!")
    return -1

def validate(routes):
    nodes = []
    failed_pairs = []
    for route in routes:
        if "node" in route:
            nodes.append(route["node"])
    
    for fnode in nodes:
        for tnode in nodes:
            res = attempt_route(routes, fnode, tnode)
            if res < 0:
                failed_pairs.append((fnode, tnode))
    
    if len(failed_pairs) > 0:
        print("Validation failed!")
        print("Could not connect the following devices:")
        print(failed_pairs)
        return
    print("Validated!")

def main():
    routes = {}
    print("===================================")
    print("=          CSP Validator          =")
    print("===================================")
    print("")
    while(True):
        value = input("> ")

        args = value.split(' ')

        match args[0]:
            case "help":
                print("===================================")
                print("=          CSP Validator          =")
                print("===================================")
                print("")
                print("Commands:")
                print("help - This help text")
                print("info <node> - Print CSP node information")
                print("load <yaml> - Load a yaml with a config of CSP nodes and their routing tables")
                print("check <from> <to> - check if there is a valid route between 'from' and 'to' nodes")
                print("validate - validate the current config")
                print("quit - Exit the program")
                print("===================================")
                print("")
            case "info":
                get_info(routes, int(args[1]))
            case "load":
                with open(args[1]) as stream:
                    try:
                        yaml_object = yaml.safe_load(stream)
                        debug_print(yaml_object)
                        routes = process_routes(yaml_object)
                        routes = parse_rtable(routes)
                    except yaml.YAMLError as exc:
                        print("Error loading yaml! " + exc)
                        pass
            case "check":
                attempt_route(routes, int(args[1]), int(args[2]))
            case "quit":
                sys.exit(0)
            case "validate":
                validate(routes)
            case _:
                print("Unknown command!")


if __name__ == '__main__':
    main() 